package com.up.map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-12 15:38
 **/
public class HashMapDemo {

    public static void main(String[] args) {
        /**
         * 创建HashMap 对象并不直接分配内存空间
         */
        Map<String,Integer> hashMap = new HashMap<>();
        /**
         * 在调用put方法的时候才会真正调用 resize() 方法
         */
        hashMap.put("1", 1);
        /**
         *
         *     public V put(K key, V value) {
         *         return putVal(hash(key), key, value, false, true);
         *     }
         *
         *     final V putVal(int hash, K key, V value, boolean onlyIfAbsent, boolean evict) {
         *     Node<K,V>[] tab; Node<K,V> p; int n, i;
         *     if ((tab = table) == null || (n = tab.length) == 0)
         *         n = (tab = resize()).length;
         *     if ((p = tab[i = (n - 1) & hash]) == null)
         *         tab[i] = newNode(hash, key, value, null);
         *     else {
         *         Node<K,V> e; K k;
         *         if (p.hash == hash &&
         *             ((k = p.key) == key || (key != null && key.equals(k))))
         *             e = p;
         *         else if (p instanceof TreeNode)
         *             e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
         *         else {
         *             for (int binCount = 0; ; ++binCount) {
         *                 if ((e = p.next) == null) {
         *                     p.next = newNode(hash, key, value, null);
         *                     if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
         *                         treeifyBin(tab, hash);
         *                     break;
         *                 }
         *                 if (e.hash == hash &&
         *                     ((k = e.key) == key || (key != null && key.equals(k))))
         *                     break;
         *                 p = e;
         *             }
         *         }
         *         if (e != null) { // existing mapping for key
         *             V oldValue = e.value;
         *             if (!onlyIfAbsent || oldValue == null)
         *                 e.value = value;
         *             afterNodeAccess(e);
         *             return oldValue;
         *         }
         *     }
         *     ++modCount;
         *     if (++size > threshold)
         *         resize();
         *     afterNodeInsertion(evict);
         *     return null;
         *
         */
        hashMap.get("");
        /**
         *     public V get(Object key) {
         *         Node<K,V> e;
         *         return (e = getNode(hash(key), key)) == null ? null : e.value;
         *     }
         */
        hashMap.remove("");
        /**
         *     public V remove(Object key) {
         *         Node<K,V> e;
         *         return (e = removeNode(hash(key), key, null, false, true)) == null ? null : e.value;
         *     }
         */

        hashMap.clear();
        /**
         *   public void clear() {
         *         Node<K,V>[] tab;
         *         modCount++;
         *         if ((tab = table) != null && size > 0) {
         *             size = 0;
         *             for (int i = 0; i < tab.length; ++i)
         *                 tab[i] = null;
         *         }
         *     }
         */
    }
}
