package com.up.map;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-12 21:02
 **/
public class LinkedHashMapDemo {

    public static void main(String[] args) {
        /**
         * LinkedHashMap 继承了HashMap ,其初始化方法是直接调用父级的初始化方法
         * 但是在 HashMap 中这些方法都是空的，就是为了允许 LinkedHashMap 的操作
         *     // Callbacks to allow LinkedHashMap post-actions
         *     void afterNodeAccess(Node<K,V> p) { }
         *     void afterNodeInsertion(boolean evict) { }
         *     void afterNodeRemoval(Node<K,V> p) { }
         */
        Map<String,Object> map = new LinkedHashMap<>();
        map.put("1", 1);
        
//        
        List<Object> objects = Collections.emptyList();
    }
}
