package com.up.list;

import java.util.LinkedList;
import java.util.List;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-11 21:40
 **/
public class PersonList {

    public static void main(String[] args) {
        List<PersonBean> personList = new LinkedList<>();
        for (int i = 0; i <10 ; i++) {
            personList.add(new PersonBean(String.valueOf(i), i%2==0?1:2,180 ));
        }

        /**
         * 比较其实就是调用了对象的 equals 方法，如果不重写 equals 则比较的就是对象的地址
         *
         *  @Override
         *    public int indexOf(Object o) {
         *        E[] a = this.a;
         *        if (o == null) {
         *            for (int i = 0; i < a.length; i++)
         *                if (a[i] == null)
         *                    return i;
         *        } else {
         *            for (int i = 0; i < a.length; i++)
         *                if (o.equals(a[i]))
         *                    return i;
         *        }
         *        return -1;
         *    }
         *
         *    public boolean equals(Object obj) {
         *         return (this == obj);
         *     }
         */

        /**
         * 而重写 equals 也需要重写 hashCode 方法
         * Note that it is generally necessary to override the {@code hashCode}
         * method whenever this method is overridden, so as to maintain the
         * general contract for the {@code hashCode} method, which states
         * that equal objects must have equal hash codes.
         *
         *  public boolean equals(Object obj) {
         *         return (this == obj);
         *     }
         */
        personList.contains(new PersonBean(String.valueOf(2), 1,180 ));


    }
}
