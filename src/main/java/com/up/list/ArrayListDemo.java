package com.up.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-10 21:55
 * @JDK 14
 **/
public class ArrayListDemo {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        /**
         *     private void add(E e, Object[] elementData, int s) {
         *         if (s == elementData.length)
         *             elementData = grow();
         *         elementData[s] = e;
         *         size = s + 1;
         *     }
         */

        Iterator<Integer> iterator = list.iterator();


        while (iterator.hasNext()){
            /**
             * public boolean hasNext() {
             *    return cursor != size;
             * }
             */

            list.remove(list.get(0));
            /**
             *
             * 这样会报异常，因为数组的长度被改变，当然不能正常按照原来的参数完整遍历
             *   public boolean remove(Object o) {
             *         final Object[] es = elementData;
             *         final int size = this.size;
             *         int i = 0;
             *         found: {
             *             if (o == null) {
             *                 for (; i < size; i++)
             *                     if (es[i] == null)
             *                         break found;
             *             } else {
             *                 for (; i < size; i++)
             *                     if (o.equals(es[i]))
             *                         break found;
             *             }
             *             return false;
             *         }
             *         fastRemove(es, i);
             *         return true;
             *     }
             *
             *   private void fastRemove(Object[] es, int i) {
             *         modCount++;
             *         final int newSize;
             *         if ((newSize = size - 1) > i)
             *             System.arraycopy(es, i + 1, es, i, newSize - i);
             *         es[size = newSize] = null;
             *     }
             */



            Integer next = iterator.next();
            /**
             * 其本质就是俩个指针在移动
             *         public E next() {
             *             checkForComodification();
             *             int i = cursor;
             *             if (i >= size)
             *                 throw new NoSuchElementException();
             *             Object[] elementData = ArrayList.this.elementData;
             *             if (i >= elementData.length)
             *                 throw new ConcurrentModificationException();
             *             cursor = i + 1;
             *             return (E) elementData[lastRet = i];
             *         }
             */
            iterator.remove();

            /**
             * 这样则会直接报错，没商量
             *     default void remove() {
             *         throw new UnsupportedOperationException("remove");
             *     }
             */
        }

//        PS 如果需要删除，可以使用 list.listIterator();
        ListIterator<Integer> integerListIterator = list.listIterator();
        while (integerListIterator.hasNext()){
            integerListIterator.remove();
            /**
             * 为啥它不报错？
             * 因为这个臭不要脸的动了指针，就不会出现索引问题
             *  public void remove() {
             *             if (this.lastRet < 0) {
             *                 throw new IllegalStateException();
             *             } else {
             *                 this.checkForComodification();
             *                 try {
             *                     ArrayList.this.remove(this.lastRet);
//             *                     指针上移
             *                     this.cursor = this.lastRet;
//             *                     重置（下次移动的时候就会更换为正确的值）
             *                     this.lastRet = -1;
             *                     this.expectedModCount = ArrayList.this.modCount;
             *                 } catch (IndexOutOfBoundsException var2) {
             *                     throw new ConcurrentModificationException();
             *                 }
             *             }
             *         }
             */
        }
    }
}
