package com.up.list;

import java.util.Vector;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-10 21:59
 **/

/**
 * Vector 也是使用数组实现的
 * ArrayList 是线程不安全的，效率高，
 * private void add(E e, Object[] elementData, int s) {
 *         if (s == elementData.length)
 *             elementData = grow();
 *         elementData[s] = e;
 *         size = s + 1;
 *     }
 * Vector 是线程安全的 效率低
 * public synchronized boolean add(E e) {
 *         modCount++;
 *         add(e, elementData, elementCount);
 *         return true;
 *     }
 * ArrayList 在扩容的时候，扩容1.5倍，
 *
 *   private Object[] grow(int minCapacity) {
 *         int oldCapacity = elementData.length;
 *         if (oldCapacity > 0 || elementData != DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
 *             int newCapacity = ArraysSupport.newLength(oldCapacity,  minCapacity - oldCapacity,oldCapacity >> 1 );
 *             return elementData=Arrays.copyOf(elementData,newCapacity);
 *          }else{
 *              return elementData=new Object[Math.max(DEFAULT_CAPACITY,minCapacity)];
 *          }
 *  }
 *
 * Vector 扩容是原来的2倍
 *
 *      *
 *      * The amount by which the capacity of the vector is automatically
 *      * incremented when its size becomes greater than its capacity.  If
 *      * the capacity increment is less than or equal to zero, the capacity
 *      * of the vector is doubled each time it needs to grow.
 *      *
 *      * @serial
 *      *
 *      protected int capacityIncrement;
 *
 *
 *
 *     private Object[] grow(int minCapacity) {
 *         int oldCapacity = elementData.length;
 *         int newCapacity = ArraysSupport.newLength(oldCapacity,minCapacity - oldCapacity,capacityIncrement>0?capacityIncrement:oldCapacity);
 *         return elementData=Arrays.copyOf(elementData,newCapacity);
 *      }
 */
public class VectorDemo {


    public static void main(String[] args) {
        Vector<Integer> list = new Vector<Integer>();
        list.add(1);
    }
}
