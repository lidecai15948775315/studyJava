## ArrayList 与 LinkedList 的区别
+ ArrayList 和 LinkedList 的区别本质上就是Array 和 Linked 的区别
    + Array 是数组，有下标，Linked 是链表，有指针，
    + 有下标的查找快，因为可以直接定位位置，没有下标的相对较慢，因为需要遍历
    + 有指针的插入快，因为只需要修改指向，没有指针的插入相对较慢，因为要移动元素
    + ArrayList 插入数据是根据长度指定了位置
    
    ```java
  /**
  * @param s size 数组大小
  */
        private void add(E e, Object[] elementData, int s) {
            if (s == elementData.length)
                elementData = grow();
            elementData[s] = e;
            size = s + 1;
        }
    ```
  
  + LinkedList 是记录了指针
  
  ```java
    void linkLast(E e) {
        final Node<E> l = last;
        final Node<E> newNode = new Node<>(l, e, null);
        last = newNode;
        if (l == null)
            first = newNode;
        else
            l.next = newNode;
        size++;
        modCount++;
    }
    ```
  
## ArrayList 与 Vector 的区别
  +  Vector 也是使用数组实现的
  +  ArrayList 是线程不安全的，效率高
      ```java
        private void add(E e, Object[] elementData, int s) {
            if (s == elementData.length)
                elementData = grow();
            elementData[s] = e;
            size = s + 1;
        }
       ```


  + Vector 是线程安全的 效率低
      ```java
        public synchronized boolean add(E e) {
                modCount++;
                add(e, elementData, elementCount);
                return true;
        }
      ```
  
 + ArrayList 在扩容的时候，扩容1.5倍，
    ```java
         private Object[] grow(int minCapacity) {
               int oldCapacity = elementData.length;
               if (oldCapacity > 0 || elementData != DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
                   int newCapacity = ArraysSupport.newLength(oldCapacity,  minCapacity - oldCapacity,oldCapacity >> 1 );
                   return elementData=Arrays.copyOf(elementData,newCapacity);
                }else{
                    return elementData=new Object[Math.max(DEFAULT_CAPACITY,minCapacity)];
                }
        }
    ```
 + Vector 扩容是原来的2倍
    ```java
        /*
        * The amount by which the capacity of the vector is automatically
        * incremented when its size becomes greater than its capacity.  If
        * the capacity increment is less than or equal to zero, the capacity
        * of the vector is doubled each time it needs to grow.
        *
        * @serial
        */
           protected int capacityIncrement;
           private Object[] grow(int minCapacity) {
               int oldCapacity = elementData.length;
               int newCapacity = ArraysSupport.newLength(oldCapacity,minCapacity - oldCapacity,capacityIncrement>0?capacityIncrement:oldCapacity);
               return elementData=Arrays.copyOf(elementData,newCapacity);
            }
    ```