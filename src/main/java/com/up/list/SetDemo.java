package com.up.list;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-11 22:47
 **/
public class SetDemo {

    public static void main(String[] args) {

        /**
         * Set 中的数据，无序，不可重复，无法根据索引查找
         */
        Set<Integer> set = new HashSet<>();
        set.add(1);
        /**
         * set.add 本质上使用的是 HashMap 的 Key
         */

        /**
         * 红黑树演示地址
         * https://rbtree.phpisfuture.com/
         */
        TreeSet<Integer> treeSet = new TreeSet<>();
        treeSet.add(1);
        /**
         * TreeSet 使用的是 TreeMap
         */
    }
}
