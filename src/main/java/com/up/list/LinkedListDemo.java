package com.up.list;

import java.util.LinkedList;
import java.util.List;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 23:06
 **/
public class LinkedListDemo {

    public static void main(String[] args) {
        List<Object> list = new LinkedList<>();
        list.add(0, "");
        list.add(0);
//        list.add() 使用的就是linkLast方法，及即在最后插入元素

        /**
         *   public void add(int index, E element) {
         *         checkPositionIndex(index);
         *
         *         if (index == size)
         *             linkLast(element);
         *         else
         *             linkBefore(element, node(index));
         *     }
         */
//        ------------------------------------------------
        /**
         *     void linkLast(E e) {
         *         final Node<E> l = last;
         *         final Node<E> newNode = new Node<>(l, e, null);
         *         last = newNode;
         *         if (l == null)
         *             first = newNode;
         *         else
         *             l.next = newNode;
         *         size++;
         *         modCount++;
         *     }
         */
//          --------------------------------------------------
        /**
         *     void linkBefore(E e, Node<E> succ) {
         *         final Node<E> pred = succ.prev;
         *         final Node<E> newNode = new Node<>(pred, e, succ);
         *         succ.prev = newNode;
         *         if (pred == null)
         *             first = newNode;
         *         else
         *             pred.next = newNode;
         *         size++;
         *         modCount++;
         *     }
         *
         *     ------------------------------------------------------------
         *         public void add(int index, E element) {
         *         rangeCheckForAdd(index);
         *         modCount++;
         *         final int s;
         *         Object[] elementData;
         *         if ((s = size) == (elementData = this.elementData).length)
         *             elementData = grow();
         *         System.arraycopy(elementData, index,
         *                          elementData, index + 1,
         *                          s - index);
         *         elementData[index] = element;
         *         size = s + 1;
         *     }
         *
         *
         *
         */
    }
}
