package com.up.list;

import java.util.Objects;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-11 21:39
 **/
public class PersonBean {
    private  String name;
    private int sex;
    private int height;


    public PersonBean() {
    }

    public PersonBean(String name, int sex, int height) {
        this.name = name;
        this.sex = sex;
        this.height = height;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public String toString() {
        return "personBean{" +
                "name='" + name + '\'' +
                ", sex=" + sex +
                ", height=" + height +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonBean that = (PersonBean) o;
        return sex == that.sex &&
                height == that.height &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sex, height);
    }
}
