package com.up.annotations;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 22:19
 **/
public class AnnotationDemo {


//    使用注解并定义需要的参数后必须填写参数才能使用
    @MyAnnotations(name = "", age = 1, id = 1, likes = {"", ""})
    public static void method() {

    }

//    如果参数有了默认值则参数不是必填
    @MyAnnotations
    public static void method2() {

    }
}
