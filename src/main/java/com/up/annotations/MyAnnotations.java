package com.up.annotations;

import java.lang.annotation.*;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 22:17
 **/
//使用范围,用来声明当前自定义的注解适合用于什么地方（类，方法，变量，包 。。。。。。）
@Target({ElementType.METHOD, ElementType.TYPE})
//在什么环境下生效（）,一般是运行时环境
@Retention(RetentionPolicy.CLASS)
//是否显示在 javadoc 中
@Documented
//表示当前注解是否能够被继承
@Inherited
public @interface MyAnnotations {

    //    定义的方式看起来是方法，实际上是在使用注解的时候填写的参数的名称，默认是value
    String name() default "繁星";

    int age() default 25;

    int id() default 1024;

    String[] likes() default {"",""};
}
