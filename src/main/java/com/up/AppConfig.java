package com.up;

import com.up.controller.IndexController;
import com.jfinal.config.*;
import com.jfinal.server.undertow.UndertowServer;
import com.jfinal.template.Engine;

public class AppConfig  extends JFinalConfig {
    @Override
    public void configConstant(Constants constants) {
        constants.setBaseUploadPath("/");

    }

    @Override
    public void configRoute(Routes routes) {
        routes.add("/", IndexController.class);

    }

    @Override
    public void configEngine(Engine engine) {

    }

    @Override
    public void configPlugin(Plugins plugins) {

    }

    @Override
    public void configInterceptor(Interceptors interceptors) {

    }

    @Override
    public void configHandler(Handlers handlers) {

    }

    public static void main(String[] args) {
        UndertowServer.start(AppConfig.class, 8080, true);
    }
}
