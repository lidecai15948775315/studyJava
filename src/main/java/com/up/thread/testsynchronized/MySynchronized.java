package com.up.thread.testsynchronized;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-11-28 17:52
 **/
public class MySynchronized {

    public void run() {
        synchronized (this) {
            for (int i = 0; i < 10; i++) {
                System.err.println(i);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void run2() {
        System.err.println("run2");
    }


}
