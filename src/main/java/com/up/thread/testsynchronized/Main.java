package com.up.thread.testsynchronized;


/**
 * @author: 李德才
 * @description:
 * @create: 2020-11-28 17:53
 **/
public class Main {
    public static void main(String[] args) throws InterruptedException {
        MySynchronized mySynchronized = new MySynchronized();
        new Thread(mySynchronized::run).start();
        Thread.sleep(1000);
        new Thread(mySynchronized::run2).start();
    }
}
