package com.up.thread.pc3;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 22:47
 **/
public class Consumer implements Runnable {
    private Goods goods;

    public Consumer(Goods goods) {
        this.goods = goods;
    }

    @Override
    public void run() {
        for(int i = 0;i<10;i++){
            goods.get();
        }
    }

}
