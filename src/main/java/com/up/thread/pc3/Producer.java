package com.up.thread.pc3;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 22:48
 **/
public class Producer implements Runnable {

    private Goods goods;

    public Producer(Goods goods) {
        this.goods = goods;
    }


    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            if (i % 2 == 0) {
                goods.set("娃哈哈", "矿泉水");
            } else {
                goods.set("旺仔", "小馒头");
            }
        }
    }

}
