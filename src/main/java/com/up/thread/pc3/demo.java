package com.up.thread.pc3;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 22:48
 **/
public class demo {
    /**
     * 在 Goods 中加入标志，用标志判断等待和唤醒
     * @param args
     */
    public static void main(String[] args) {
//
        Goods goods = new Goods();
        Producer producer = new Producer(goods);
        Consumer consumer = new Consumer(goods);

        Thread t1 = new Thread(producer);
        Thread t2 = new Thread(consumer);
        t1.start();
        t2.start();


    }

}
