package com.up.thread.pc2;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 22:48
 **/
public class demo {
    /**
     *  将生产和消费使用 synchronized 限制访问，保证原子性
     *  但是还是会出现生产者还没有生产，消费者就要取走的情况
     * @param args
     */
    public static void main(String[] args) {
//
        Goods goods = new Goods();
        Producer producer = new Producer(goods);
        Consumer consumer = new Consumer(goods);

        Thread t1 = new Thread(producer);
        Thread t2 = new Thread(consumer);
        t1.start();
        t2.start();


    }

}
