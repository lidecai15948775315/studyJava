package com.up.thread.pool;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.*;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-14 21:48
 **/
public class ThreadPoolDemo {

    /**
     * 线程池执行过程
     * 提交任务 _> 核心线程池是否已满 _> 阻塞队列是否以满 _> 线程池是否已满 _> 按照包和策略处理
     * ***********创建线程执行任务 _> 将任务存储到队列中 _> 创建线程执行任务
     */

    public static void main(String[] args) {

//        newCachedThreadPool();
//        newScheduledThreadPool();
//        newFixedThreadPool();
//        newWorkStealingPool();
//        newSingleThreadExecutor();
        newScheduledThreadPool();
    }


    //        设置并行线程数的线程池
    public static void newWorkStealingPool() {
        ExecutorService executorService1 = Executors.newWorkStealingPool(10);

        executorService1.submit(() -> {
            System.out.println("线程" + Thread.currentThread() + "完成任务：  时间为：" + new Date().getSeconds());
        });
        while (true) {
            //主线程陷入死循环，来观察结果，否则是看不到结果的
        }
//        executorService1.shutdown();
    }

    //        延迟执行
    public static void newScheduledThreadPool() {
        ScheduledExecutorService scheduledExecutorService1 = Executors.newScheduledThreadPool(10);
        scheduledExecutorService1.schedule(() -> {
            System.err.println("延迟三秒");
        }, 3, TimeUnit.SECONDS);
        scheduledExecutorService1.shutdown();

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(2);
        scheduledExecutorService.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                System.out.println("延迟一秒执行，每三秒执行一次");
            }
        }, 1, 3, TimeUnit.SECONDS);
    }

    public static void newSingleThreadExecutor() {
        //        创建只有单个线程的线程池,线程池只有一个线程，其他线程在队列中等待
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            System.err.println("----------------------");
        });
        executorService.shutdown();
    }

    //        创建固定长度的线程池
    public static void newFixedThreadPool() {
        ExecutorService pool = Executors.newFixedThreadPool(10);
        pool.execute(() -> {
            System.err.println("------------");
        });
        pool.shutdown();
    }

    //        弹性的线程池
    public static void newCachedThreadPool() {
//        ExecutorService executorService = Executors.newCachedThreadPool();
//        executorService.execute(() -> {
//            System.err.println("----------------------");
//        });
//        executorService.shutdown();

        //        在需要的时候使用线程工厂创建新线程
        ExecutorService executorService1 = Executors.newCachedThreadPool(r -> new Thread(UUID.randomUUID().toString()));
        Runnable runnable = () -> {
            System.err.println("-----------------");
        };
        Future<?> submit = executorService1.submit(runnable);
        try {
            submit.get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executorService1.shutdown();
    }


    /**
     * 线程池真相
     */
    public static void truth() {
        /**
         * 线程工厂创建新线程
         */
        ThreadFactory threadFactor = Thread::new;
        /**
         * 拒绝策略
         * ThreadPoolExecutor.AbortPolicy:丢弃任务并抛出RejectedExecutionException异常。
         * ThreadPoolExecutor.DiscardPolicy：也是丢弃任务，但是不抛出异常。
         * ThreadPoolExecutor.DiscardOldestPolicy：丢弃队列最前面的任务，然后重新尝试执行任务（重复此过程）
         * ThreadPoolExecutor.CallerRunsPolicy：由调用线程处理该任务
         */
        RejectedExecutionHandler rejectedExecutionHandler = (r, executor) -> {
            ThreadPoolExecutor.AbortPolicy abortPolicy = new ThreadPoolExecutor.AbortPolicy();
            abortPolicy.rejectedExecution(() -> {
                System.err.println("我拒绝");
            }, executor);
        };

        /**
         * 队列
         * BlockingQueue
         * new LinkedBlockingDeque<>()
         * new DelayQueue<>()
         * new PriorityQueue<>()
         * new SynchronousQueue<>()
         */

        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(10, 50, 50L, TimeUnit.MINUTES, new ArrayBlockingQueue<>(5), threadFactor, rejectedExecutionHandler);
        threadPoolExecutor.execute(() -> {
            System.err.println("*************************");
        });

        /**
         *
         * @param corePoolSize 核心线程数，没有任务也会存在   newCachedThreadPool() 的核心线程数是 0
         * @param maximumPoolSize 最大线程数
         * @param keepAliveTime 线程完成任务后存活时间，避免其他任务重新创建线程
         * @param TimeUnit 存活时间的单位
         * @param BlockingQueue<Runnable> 阻塞队列，没有空闲 ***核心线程*** 的时候任务会到阻塞队列
         * @param ThreadFactory 创建线程的工厂，
         * @param RejectedExecutionHandler    饱和策略/拒绝策略
         * public ThreadPoolExecutor(int corePoolSize,
         *                               int maximumPoolSize,
         *                               long keepAliveTime,
         *                               TimeUnit unit,
         *                               BlockingQueue<Runnable> workQueue,
         *                               ThreadFactory threadFactory,
         *                               RejectedExecutionHandler handler)
         */
    }

}
