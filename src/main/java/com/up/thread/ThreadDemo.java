package com.up.thread;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 20:55
 **/
public class ThreadDemo {

    public static void main(String[] args) {
        theSame();
    }

    /**
     * 这其实是一个意思 。。。 。。。
     */
    public static void  theSame()   {
        new Thread(() -> {
            System.err.println("---------------------");
        }).start();
        new Thread(new Impl_Runnable()).start();
    }

    public void  getReturn(){
        Impl_Callable impl_callBale = new Impl_Callable();
        new Thread(String.valueOf(impl_callBale)).start();
        ExecutorService executor = Executors.newCachedThreadPool();
        Future<Integer> submit = executor.submit(impl_callBale);
        try {
            System.err.println(submit.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
