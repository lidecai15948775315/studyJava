package com.up.thread.pc4;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 23:02
 **/
public class Goods {

    private String brand;
    private String name;

    /**
     * 使用构造方法保证创建实例的原子性
     * @param brand
     * @param name
     */
    public Goods(String brand, String name) {
        this.brand = brand;
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
