package com.up.thread.pc4;

import java.util.concurrent.BlockingQueue;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 23:02
 **/
public class ProducerQueue implements Runnable {

    private BlockingQueue<Goods> blockingQueue;

    public ProducerQueue(BlockingQueue<Goods> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            Goods goods ;
            if (i % 2 == 0) {
                goods = new Goods("娃哈哈", "矿泉水");
            } else {
                goods = new Goods("旺仔", "小馒头");
            }
            System.out.println("生产者开始生产商品：" + goods.getBrand() + "--" + goods.getName());
            try {
//                将商品存入队列
                blockingQueue.put(goods);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
