package com.up.thread.pc4;

import java.util.concurrent.BlockingQueue;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 23:01
 **/
public class ConsumerQueue implements Runnable {

    private  BlockingQueue<Goods> blockingQueue;

    public ConsumerQueue(BlockingQueue<Goods> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        for(int i = 0;i<10;i++){
            try {
//                从队列中获取
                Goods goods = blockingQueue.take();
                System.out.println("消费者消费的商品是："+goods.getBrand()+"--"+goods.getName());
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
