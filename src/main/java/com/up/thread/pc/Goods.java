package com.up.thread.pc;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 22:47
 **/
public class Goods {
    private String brand;
    private String name;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
