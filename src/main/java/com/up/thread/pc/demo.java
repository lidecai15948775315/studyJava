package com.up.thread.pc;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 22:48
 **/
public class demo {
    /**
     * 消费者取走了娃哈哈----null
     * 由于没有任何权限和顺序控制，会出现还没生产，消费者就来获取的情况
     * 消费者取走了旺仔----矿泉水
     * 生产者的生产不是原子性的，会出现品牌和名称对不上的情况
     * @param args
     */
    public static void main(String[] args) {
        Goods goods = new Goods();
        Producer producer = new Producer(goods);
        Consumer consumer = new Consumer(goods);
        Thread t1 = new Thread(producer);
        Thread t2 = new Thread(consumer);
        t1.start();
        t2.start();
    }

}
