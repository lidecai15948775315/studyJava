package com.up.thread;

import java.util.concurrent.Callable;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-13 21:26
 **/
public class Impl_Callable implements Callable {
    @Override
    public Object call() throws Exception {
        System.err.println("---------------");
        return 1;
    }
}
