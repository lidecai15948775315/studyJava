package com.up.cup;

/**
 * @author: 李德才
 * @description: cpu 乱序执行
 * @create: 2020-08-08 22:18
 * https://blog.csdn.net/qq_36623327/article/details/107622833
 **/
public class CpuDisorderlyExecution {

    private static int x = 0, y = 0;
    private static int a = 0, b = 0;

    public static void main(String[] args) throws InterruptedException {
        int i = 0;
        boolean flag = true;
        while (flag) {
            i++;
            x = 0;
            y = 0;
            a = 0;
            b = 0;
            Thread one = new Thread(() -> {
                a = 1;
                x = b;
            });
            Thread other = new Thread(() -> {
                b = 1;
                y = a;
            });
            one.start();
            other.start();
            one.join();
            other.join();
            String result = "第" + i + "次 (" + x + "," + y + "）";
            if (x == 0 && y == 0) {
                System.err.println(result);
                flag = false;
            }
        }
    }
}
