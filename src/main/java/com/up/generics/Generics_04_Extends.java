package com.up.generics;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * 泛型的用法非常灵活，除在集合、类和方法中使用外，本节将从三个方面介绍泛型的高级用法，
 * 包括限制泛型可用类型、使用类型通配符、继承泛型类和实现泛型接口
 */
public class Generics_04_Extends<T extends List> {


    public static void main(String[] args) {
        // 实例化使用ArrayList的泛型类ListClass，正确
        Generics_04_Extends<ArrayList> lc1 = new Generics_04_Extends<ArrayList>();
        // 实例化使用LinkedList的泛型类LlstClass，正确
        Generics_04_Extends<LinkedList> lc2 = new Generics_04_Extends<LinkedList>();
        // 实例化使用HashMap的泛型类ListClass，错误，因为HasMap没有实现List接口
        // ListClass<HashMap> lc3=new ListClass<HashMap>();
    }
}
