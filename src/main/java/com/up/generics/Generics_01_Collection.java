package com.up.generics;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 泛型集合
 */

public class Generics_01_Collection {


    /**
     * Java 集合有个缺点，就是把一个对象“丢进”集合里之后，集合就会“忘记”这个对象的数据类型，当再次取出该对象时，
     * 该对象的编译类型就变成了 Object 类型（其运行时类型没变）
     * <p>
     * 从 Java 1.5 开始提供了泛型。泛型可以在编译的时候检查类型安全，并且所有的强制转换都是自动和隐式的，提高了代码的重用率
     * <p>
     * 泛型本质上是提供类型的 “类型参数”，也就是参数化类型。我们可以为类、接口或方法指定一个类型参数，
     * 通过这个参数限制操作的数据类型，从而保证类型转换的绝对安全。
     */

    public static void main(String[] args) {
        // 创建3个Book对象
        Book book1 = new Book(1, "唐诗三百首", 8);
        Book book2 = new Book(2, "小星星", 12);
        Book book3 = new Book(3, "成语大全", 22);
        Map<Integer, Book> books = new HashMap<Integer, Book>(); // 定义泛型 Map 集合
        books.put(1001, book1); // 将第一个 Book 对象存储到 Map 中
        books.put(1002, book2); // 将第二个 Book 对象存储到 Map 中
        books.put(1003, book3); // 将第三个 Book 对象存储到 Map 中
        System.out.println("泛型Map存储的图书信息如下：");
        for (Integer id : books.keySet()) {
            // 遍历键
            System.out.print(id + "——");
            System.out.println(books.get(id)); // 不需要类型转换
        }
        List<Book> bookList = new ArrayList<Book>(); // 定义泛型的 List 集合
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);
        System.out.println("泛型List存储的图书信息如下：");
        for (int i = 0; i < bookList.size(); i++) {
            System.out.println(bookList.get(i)); // 这里不需要类型转换
        }
    }

    public static class Book {
        private int Id; // 图书编号
        private String Name; // 图书名称
        private int Price; // 图书价格

        public Book(int id, String name, int price) { // 构造方法
            this.Id = id;
            this.Name = name;
            this.Price = price;
        }

        public String toString() { // 重写 toString()方法
            return this.Id + ", " + this.Name + "，" + this.Price;
        }
    }
}
