package com.up.generics;

public class Generics_06_ExtendsGenerics {


    /**
     * 定义为泛型的类和接口也可以被继承和实现
     * 如果要在 SonClass 类继承 FatherClass 类时保留父类的泛型类型，需要在继承时指定，
     * 否则直接使用 extends FatherClass 语句进行继承操作，此时 T1、T2 和 T3 都会自动变为 Object，
     * 所以一般情况下都将父类的泛型类型保留
     */

    public class FatherClass<T1> {
    }

    public class SonClass<T1, T2, T3> extends FatherClass<T1> {

    }
}
