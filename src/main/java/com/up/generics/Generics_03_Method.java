package com.up.generics;


public class Generics_03_Method {


    /**
     * 泛型方法使得该方法能够独立于类而产生变化。如果使用泛型方法可以取代类泛型化，那么就应该只使用泛型方法。
     * 另外，对一个 static 的方法而言，无法访问泛型类的类型参数。
     * 因此，如果 static 方法需要使用泛型能力，就必须使其成为泛型方法
     */

    public static <T> void List(T book) { // 定义泛型方法
        if (book != null) {
            System.out.println(book);
        }
    }

    public static void main(String[] args) {
        Generics_01_Collection.Book stu = new Generics_01_Collection.Book(1, "细学 Java 编程", 28);
        List(stu); // 调用泛型方法
    }
}
