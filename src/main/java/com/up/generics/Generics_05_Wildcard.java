package com.up.generics;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


/**
 * Java 中的泛型还支持使用类型通配符，它的作用是在创建一个泛型类对象时限制这个泛型类的类型必须实现或继承某个接口或类。
 */

public class Generics_05_Wildcard {

    public static void main(String[] args) {
        A<? extends List> a = null;
        a = new A<ArrayList>();    // 正确
        a = new A<LinkedList>();    // 正确
//        c = new A<HashMap> ();    // 错误
    }


    public static class A<T extends List> {

    }
}
