package com.up.generics;


/**
 * 除了可以定义泛型集合之外，还可以直接限定泛型类的类型参数
 */
public class Generics_02_Class<data_type1, data_type2> {

    /**
     * 该语句中的 data_type1 与类声明中的 data_type1 表示的是同一种数据类型
     */
    private data_type1 property_name1;
    private data_type2 property_name2;


    public static class Stu<N> {
        private N name; // 姓名

        public Stu(N name) {
            this.name = name;
        }

        // 下面是上面3个属性的setter/getter方法
        public N getName() {
            return name;
        }

        public void setName(N name) {
            this.name = name;
        }
    }

    public static void main(String[] args) {
        Stu<String> stu = new Stu<String>("张晓玲");
        String name = stu.getName();
        System.out.println("学生信息如下：");
        System.out.println("学生姓名：" + name);
    }


}
