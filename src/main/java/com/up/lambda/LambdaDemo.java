package com.up.lambda;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 09:49
 **/

import java.util.ArrayList;
import java.util.List;

/**
 * 1、函数式编程
 * 2、参数类型自动判断
 * 3、代码量少、简洁
 */
public class LambdaDemo {

    /**
     * https://21yi.com/java/java-functional-programming_10410.html
     */

    /**
     * 如果接口中只有一个抽象方法（可以包含多个默认方法或多个 static 方法），那么该接口就是函数式接口
     * @FunctionalInterface 就是用来指定某个接口必须是函数式接口，所以 @FunInterface 只能修饰接口，不能修饰其它程序元素。
     * @FunctionalInterface 注解的作用只是告诉编译器检查这个接口，保证该接口只能包含一个抽象方法，否则就会编译出错
     */

    /**
     * @param args
     */
    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<Student>();
        list.add(new Student("zhangsan", 14, 67));
        list.add(new Student("lisi", 13, 89));
        list.add(new Student("wangwu", 15, 97));
        list.add(new Student("maliu", 12, 63));
        list.add(new Student("zhaoqi", 17, 75));
//       针对不同的需求使用不同的策略过滤、
//        这里相当于将函数直接传递到方法中
        filterStudent(list, (e) -> e.getAge() > 17);
//        等效于
        filterStudent(list, new StudentFilter() {
            @Override
            public boolean filter(Student student) {
                return student.getAge() > 17;
            }
        });
    }


    public static void filterStudent(ArrayList<Student> students, StudentFilter filter) {
        List<Student> list = new ArrayList<>();
        list.forEach(student -> {
            if (filter.filter(student)) {
                list.add(student);
            }
        });

        list.forEach(student -> {
            System.err.println(student.toString());
        });
    }

}
