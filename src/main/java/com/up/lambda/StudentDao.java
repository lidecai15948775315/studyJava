package com.up.lambda;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 14:34
 **/

/**
 * @FunctionalInterface 就是用来指定某个接口必须是函数式接口，所以 @FunInterface 只能修饰接口，不能修饰其它程序元素。
 * @FunctionalInterface 注解的作用只是告诉编译器检查这个接口，保证该接口只能包含一个抽象方法，否则就会编译出错
 */

@FunctionalInterface
public interface StudentDao {

    void insert(Student student);

}
