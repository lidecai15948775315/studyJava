package com.up.lambda.streamapi;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * * 腾讯云社区
 * * https://cloud.tencent.com/developer/article/1368219
 */
public class StreamApi {

    /**
     * Stream是处理数组和集合的API,Stream具有以下特点：
     * 不是数据结构，没有内部存储
     * 不支持索引访问
     * 延迟计算
     * 支持过滤，查找，转换，汇总等操作
     */

    /**
     * 对于StreamAPI的学习，首先需要弄清楚lambda的两个操作类型：中间操作和终止操作。    下面通过一个demo来认识下这个过程
     */

    public static void filter() {
        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5).filter(x -> {
            System.out.print(x);
            return x > 3;
        });
//        如果没有下面的终止操作，则不会有任何输入，程序也不会执行
        integerStream.forEach(System.err::println);

        /**
         * *****中间操作*****
         * 过滤 filter
         * 去重 distinct
         * 排序 sorted
         * 截取 limit、skip
         * 转换 map/flatMap
         * 其他 peek
         *  *****终止操作*****
         * 循环 forEach
         * 计算 min、max、count、 average
         * 匹配 anyMatch、 allMatch、 noneMatch、 findFirst、 findAny
         * 汇聚 reduce
         * 收集器 toArray collect
         */
    }

    /**
     * 常用表达式
     */

    public static void main(String[] args) {

        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student("黎  明", 20, new BigDecimal(80), 1));
        studentList.add(new Student("郭敬明", 22, new BigDecimal(90), 2));
        studentList.add(new Student("明  道", 21, new BigDecimal(65.5), 3));
        studentList.add(new Student("郭富城", 30, new BigDecimal(90.5), 4));
        studentList.add(new Student("刘诗诗", 20, new BigDecimal(75), 1));
        studentList.add(new Student("成  龙", 60, new BigDecimal(88), 5));
        studentList.add(new Student("郑伊健", 60, new BigDecimal(86), 1));
        studentList.add(new Student("刘德华", 40, new BigDecimal(81), 1));
        studentList.add(new Student("古天乐", 50, new BigDecimal(83), 2));
        studentList.add(new Student("赵文卓", 40, new BigDecimal(84), 2));
        studentList.add(new Student("吴奇隆", 30, new BigDecimal(86), 4));
        studentList.add(new Student("言承旭", 50, new BigDecimal(68), 1));
        studentList.add(new Student("郑伊健", 60, new BigDecimal(86), 1));
        studentList.add(new Student("黎  明", 20, new BigDecimal(80), 1));
        studentList.add(new Student("李连杰", 65, new BigDecimal(86), 4));
        studentList.add(new Student("周润发", 69, new BigDecimal(58), 1));
        studentList.add(new Student("徐若萱", 28, new BigDecimal(88), 6));
        studentList.add(new Student("许慧欣", 26, new BigDecimal(86), 8));
//        forEach
        studentList.stream().forEach(x -> System.out.println(x.getStuName()));
//        filter
        studentList.stream().filter(t -> t.getScore().compareTo(new BigDecimal(80)) > 0).forEach(x -> System.out.println(x.getStuName()));
//        distinct
        studentList.stream().distinct().forEach(x -> System.out.println(x.getStuName()));
//        sorted
        studentList.stream().sorted(Comparator.comparing(Student::getScore)).forEach(x -> System.out.println(x.getStuName()));
//        comparing   thenComparing //多条件排序
        studentList.stream().sorted(Comparator.comparing(Student::getScore).thenComparing(Student::getStuName)).forEach(x -> System.out.println(x.getStuName()));
//        groupingBy
        System.err.println(studentList.stream().collect(Collectors.groupingBy(x -> x.getAge(), Collectors.counting())));
//        skip  limit
        studentList.stream().skip(10).limit(5).forEach(x -> System.out.println(x.getStuName()));
        int pageIndex = 1;
        int pageSize = 5;
//        实现分页
        studentList.stream().skip((pageIndex - 1) * pageSize).limit(pageSize).forEach(x -> System.out.println(x.getStuName()));
//        map 转换 -> mapToInt,mapToDouble
        studentList.stream().map(Student::getScore).forEach(x -> System.out.println(x));
//        flatMap
        studentList.stream().flatMap(x -> Arrays.stream(x.getStuName().split(""))).forEach(x -> System.out.println(x));
//        max
        studentList.stream().max(Comparator.comparing(x -> x.getAge())).ifPresent(x -> System.out.println(x.getAge()));
//        min
        studentList.stream().min(Comparator.comparing(x -> x.getAge())).ifPresent(x -> System.out.println(x.getAge()));
//        count
        System.out.println(studentList.stream().count());
//        average
        studentList.stream().mapToDouble(x -> x.getScore().doubleValue()).average().ifPresent(x -> System.out.println(x));
//        anyMatch  操作用于判断Stream中的是否 **有满足** 指定条件的元素。如果最少有一个满足条件返回true，否则返回false。
        System.out.println(studentList.stream().anyMatch(r -> r.getStuName().contains("伟")));
//        allMatch 操作用于判断Stream中的是 ***全部满足** 指定条件的元素。如果最少有一个满足条件返回true，否则返回false。
        System.out.println(studentList.stream().allMatch(r -> r.getStuName().contains("伟")));
//        noneMatch： 与anyMatch相反。allMatch是判断所有元素是不是都满足表达式。
        System.out.println(studentList.stream().noneMatch(r -> r.getStuName().contains("伟")));
//        findFirst  操作用于获取含有Stream中的第一个元素的Optional，如果Stream为空，则返回一个空的Optional。若Stream并未排序，可能返回含有Stream中任意元素的Optional。
        System.out.println(studentList.stream().findFirst());
//         findAny： 操作用于获取含有Stream中的某个元素的Optional，如果Stream为空，则返回一个空的Optional。由于此操作的行动是不确定的，
//         其会自由的选择Stream中的任何元素。在并行操作中，在同一个Stram中多次调用，可能会不同的结果。在串行调用时，都是获取的第一个元素，  默认的是获取第一个元素,并行是随机的返回。
        System.out.println(studentList.stream().findAny());
        for (int i = 0; i < 10; i++) {
            System.out.println(studentList.stream().parallel().findAny().get().getStuName());
        }
        /***
         * reduce是一个循环，有两个参数
         * 第一次执行的时候x是第一个值，y是第二个值。
         * 在第二次执行的时候，x是上次返回的值，y是第三个值
         *   ….  直到循环结束为止。
         *   */
        Stream.of(1, 5, 10, 8).reduce((x, y) -> {
            System.out.println("x : " + x);
            System.out.println("y : " + y);
            System.out.println("x+y : " + x);
            System.out.println("--------");
            return x + y;
        });
        System.out.println(studentList.stream().collect(Collectors.groupingBy(x -> x.getAge(), Collectors.counting())));

    }

    public static class Student {


        public Student(String stuName, int age, BigDecimal score, int clazz) {
            this.stuName = stuName;
            this.age = age;
            this.score = score;
            this.clazz = clazz;
        }

        private String stuName;
        private int age;
        private BigDecimal score;
        private int clazz;

        public String getStuName() {
            return stuName;
        }

        public void setStuName(String stuName) {
            this.stuName = stuName;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public BigDecimal getScore() {
            return score;
        }

        public void setScore(BigDecimal score) {
            this.score = score;
        }

        public int getClazz() {
            return clazz;
        }

        public void setClazz(int clazz) {
            this.clazz = clazz;
        }
    }

}
