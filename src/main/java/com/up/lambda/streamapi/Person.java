package com.up.lambda.streamapi;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 21:08
 **/
public class Person {
    private Integer age;

    public Person() {
    }

    public Person(Integer age) {
        this.age = age;
    }

    public static Person build(Integer age) {
        Person person = new Person();
        person.setAge(age);
        return person;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
