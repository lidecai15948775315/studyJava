package com.up.lambda.streamapi;


import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 19:10
 **/

/**
 * 中文教程
 * https://21yi.com/java/java.util.stream.Stream_Reduction_Operations_9882.html

 */
public class StreamDemo {



    private static List<Integer> numberList = Arrays.asList(1, 2, 3, 3, 4, 5, 6, 7, 8, 9);

    //    数组生成
    static void gen1() {
        numberList.stream().forEach(System.err::println);
    }

    //    集合生成
    static void gen2() {
        Stream<Integer> stream = numberList.stream();
        stream.forEach(System.err::println);
    }


    //    generate
    static void gen3() {
        Stream<Double> generate = Stream.generate(() -> Math.random());
//        无限流，不断输出
//        generate.forEach(System.err::println);
//        截取
        generate.limit(100L).forEach(System.err::println);
    }

    //    使用 iterator
    static void gen4() {
        Stream<Integer> iterate = Stream.iterate(1, x -> x + 1);
        iterate.limit(100L).forEach(System.err::println);
    }

    //    其他方式
    static void gen5() {
        String str = "abcdefg";
        IntStream intStream = str.chars();
        intStream.forEach(System.err::println);
    }

    public static void convert1() {
        numberList.stream().filter((x) -> x % 2 == 0).forEach(System.err::println);
    }

    public static void convert2() {
        int sum = numberList.stream().filter(x -> x % 2 == 0).mapToInt(x -> x).sum();
        System.err.println(sum);
    }

    public static void convert3() {
        Integer integer = numberList.stream().max(Comparator.comparingInt(a -> a)).get();
        System.err.println(integer);
        Integer integer1 = numberList.stream().min(Comparator.comparingInt(a -> a)).get();
        System.err.println(integer1);
    }

    public static void find() {
        Optional<Integer> any = numberList.stream().filter(x -> x % 2 == 0).findAny();
        System.err.println(any.get());
        Optional<Integer> first = numberList.stream().findFirst();
        System.err.println(first.get());


        Stream<Integer> integerStream = numberList.stream().filter(x -> {
            System.err.println("我执行了");
            return x % 2 == 0;
        });
        //        这样不会执行
        System.err.println(integerStream);
//        这样才会执行
        System.err.println(integerStream.findAny());
    }


    public static void max() {
        System.err.println(numberList.stream().sorted((a, b) -> b - a).findFirst().get());
    }

    public static void min() {
        System.err.println(numberList.stream().sorted((a, b) -> a - b).findFirst().get());
    }

    public static void getList() {
        List<Integer> collect = numberList.stream().filter(x -> x % 2 == 0).collect(Collectors.toList());
        System.err.println(collect);
    }

    public static void distinct() {
//        使用无序流源(例如{@link #generate(Supplier)})或者使用{@link #unordered()}来移除排序约束，如果你的情况语义允许，
//        可以在并行管道中显著地提高{@code distinct()}的执行效率
        System.err.println(Stream.generate(() -> numberList).distinct());
        System.err.println(numberList.stream().unordered().distinct());
        Stream<Integer> distinct = numberList.stream().distinct();
        distinct.forEach(System.err::println);
    }

    public static void distinctBySet() {
//        numberList.stream().collect(Collectors.toSet()).forEach(System.err::println);
        new HashSet<>(numberList).forEach(System.err::println);
    }


    public static void sum() {
        System.err.println(numberList.stream().mapToInt(x -> x).sum());
        System.err.println(numberList.stream().map(x -> x).mapToInt(x -> x).sum());
        System.err.println(numberList.stream().mapToInt(Integer::valueOf).sum());
    }

    public static void createPerson() {
// 使用构造方法
        numberList.stream().map(Person::new).forEach(System.err::println);
        numberList.stream().map(x -> new Person(x)).forEach(System.err::println);
// 使用普通方法
        numberList.stream().map(x -> Person.build(x)).forEach(System.err::println);
        numberList.stream().map(Person::build).forEach(System.err::println);

    }

    public static void main(String[] args) {
        flatMap();
    }

    /**
     * https://blog.csdn.net/Mark_Chao/article/details/80810030
     */
    public static void flatMap() {
        String[] words = new String[]{"Hello", "World"};
        List<String[]> a = Arrays.stream(words)
                .map(word -> word.split(""))
                .distinct()
                .collect(Collectors.toList());
        a.forEach(System.out::println);

        List<String> b = Arrays.stream(words)
                .map(word -> word.split(""))
                .flatMap(Arrays::stream)
                .distinct()
                .collect(Collectors.toList());
        System.err.println(b);
    }

    /**
     * https://blog.csdn.net/qq_31635851/article/details/111150978
     */
    public static void peek() {
        System.err.println(numberList.stream().peek(System.err::println).mapToInt(x -> x).sum());
    }


}
