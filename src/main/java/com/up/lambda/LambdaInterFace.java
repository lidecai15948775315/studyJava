package com.up.lambda;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 14:36
 **/
public class LambdaInterFace {

    public static void main(String[] args) {
        StudentDao studentDao = new StudentDao() {
            @Override
            public void insert(Student student) {
                System.err.println(student.getAge());
            }
        };
        studentDao.insert(new Student("zhangsan", 14, 67));

        StudentDao studentDao1 = (student -> {
            System.err.println(student.getAge());
        });
        studentDao1.insert(new Student("zhangsan", 14, 67));

        StudentDao studentDao2 = (student) -> System.err.println(student.getAge());
        studentDao2.insert(new Student("zhangsan", 14, 67));
    }
}
