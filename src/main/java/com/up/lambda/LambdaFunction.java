package com.up.lambda;

import java.util.function.Function;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 14:50
 **/
public class LambdaFunction {

    public static void main(String[] args) {
        Function<String, Integer> function = (str) -> str.length();
        System.err.println(function.apply("123456789"));
    }
}
