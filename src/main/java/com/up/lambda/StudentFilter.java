package com.up.lambda;


/**
 * 定义过滤器接口，用于重写当前接口中的方法，
 * 函数式接口 :
 *   一个接口中有且仅有一个方法
 */
public interface StudentFilter {

    boolean filter(Student student);
}
