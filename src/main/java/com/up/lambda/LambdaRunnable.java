package com.up.lambda;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 14:24
 **/
public class LambdaRunnable {

    public static void main(String[] args) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.err.println("126456798");
            }
        };
        runnable.run();

        Runnable runnable1 = ()->{
            System.err.println(12345789);
        };
        runnable1.run();

        Runnable runnable2 = () -> System.err.println(123465);
        runnable2.run();
    }
}
