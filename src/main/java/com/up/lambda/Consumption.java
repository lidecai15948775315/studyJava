package com.up.lambda;


import java.util.function.Consumer;
import java.util.function.Function;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-09 17:18
 **/
public class Consumption {

//    针对不同的输入输出，要采用不同的接口
    public static void main(String[] args) {
        Consumer<String> consumer = Production::getLength;
        consumer.accept("41565655");

        Function<String,Integer> num = new Production()::getLength2;
        System.err.println(num.apply("cdcscd"));
    }
}
