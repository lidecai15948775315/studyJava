package com.up.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-16 10:12
 **/

/**
 * newInstance() 需要使用无参构造方法
 */
public class ClassAPI {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
//        获取成员变量，包括子类和父类，只能包含公共的变量和方法
        Class<Student> studentClass = Student.class;
        Field[] fields = studentClass.getFields();
        Arrays.asList(fields).forEach(field -> {
            System.err.println(field.getName());
            System.err.println(field.getType());
            System.err.println(field.getModifiers());
            System.err.println("------------------------------");
        });
// ==============================================================
//        返回当前类的所有属性，不局限于访问修饰符，
        Field[] declaredFields = studentClass.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            System.err.println(declaredField.getName());
        }
// ==================================================================
//        反射在一定意义上破坏了封装性
        Field addresses = studentClass.getDeclaredField("address");
//        允许使用私有属性
        addresses.setAccessible(true);
        System.err.println(addresses.getName());
        Student student = studentClass.newInstance();
        student.setAddress("背景");
        student.setClassName("JAVA架构师");
        System.err.println(student.toString());


//        =====================================================
        //        获取该对象的普通方法,包含当前对象及父类的所有公共方法
        Method[] methods = studentClass.getMethods();
        Arrays.asList(methods).forEach(method -> {
            System.err.println(method.getName());
            System.err.println(method.getModifiers());
        });

//        获取方法
        Method[] declaredMethods = studentClass.getDeclaredMethods();
        Arrays.asList(declaredMethods).forEach(declaredMethod -> {
            System.err.println(declaredMethod.getName());
            System.err.println(declaredMethod.getModifiers());
        });

//        使用方法
        Method add = studentClass.getDeclaredMethod("add", int.class, int.class);
        add.setAccessible(true);
        Object student1 = studentClass.newInstance();
        add.invoke(student1, 22, 123);

        //只能获取共有的构造方法
        Constructor<?>[] constructors = studentClass.getConstructors();
        Arrays.asList(constructors).forEach(constructor -> {
            System.err.println(constructor.getName());
        });

// 获取所有构造方法
        Constructor<?>[] declaredConstructors = studentClass.getDeclaredConstructors();
        Arrays.asList(declaredConstructors).forEach(declaredConstructor -> {
            System.err.println(declaredConstructor.getName());
        });


// 指定构造方法
        Constructor<Student> declaredConstructor = studentClass.getDeclaredConstructor(String.class, int.class, String.class);
        declaredConstructor.setAccessible(true);
        Student student2 = declaredConstructor.newInstance("北京", 25, "JAVA 架构师");
        System.err.println(student2);
    }
}
