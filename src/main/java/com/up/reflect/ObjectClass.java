package com.up.reflect;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-16 09:56
 **/
public class ObjectClass {
    private Integer id;
    private String userName;

    public ObjectClass(Integer id, String userName) {
        this.id = id;
        this.userName = userName;
    }

    public ObjectClass() {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "ObjectClass{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                '}';
    }
}
