package com.up.reflect;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-16 09:54
 **/
public class CreateeClassObject {
    public static void main(String[] args) {


    }

    public void getClass1() throws ClassNotFoundException {
        //        通过 class.forName() 获取Class对象
        Class<?> aClass = Class.forName("com.up.reflect.ObjectClass");
        System.err.println(aClass.getPackage());
        System.err.println(aClass.getName());
        System.err.println(aClass.getSimpleName());
        System.err.println(aClass.getCanonicalName());
        Field[] fields = aClass.getFields();
        Arrays.asList(fields).forEach(field -> {
            System.err.println(field);
        });
        Method[] methods = aClass.getMethods();
        Arrays.asList(methods).forEach(method -> {
            System.err.println(method);
        });
    }

    /**
     * 通过类名
     */
    public void getClass2() {
        Class<ObjectClass> objectClassClass = ObjectClass.class;
        Arrays.asList(objectClassClass.getMethods()).forEach(method -> {
            System.err.println(method);
        });
    }

    /**
     * 通过对象
     */
    public void getClass3() {
        Class<? extends ObjectClass> aClass = new ObjectClass().getClass();
        Arrays.asList(aClass.getMethods()).forEach(method -> {
            System.err.println(method);
        });
    }
}
