package com.up.whyioc.service.impl;


import com.up.whyioc.dao.UserDao;
import com.up.whyioc.service.UserService;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-16 17:01
 **/
public class UserServiceImpl implements UserService {

    /**
     * 这种方式更换了上层架构就必须重写代码
     */
//    private UserDao userDao = new UserDaoImpl();
//    private UserDao userDao = new UserDaoMySqlImpl();

//    先创建一个空的对象
    private UserDao userDao;

    //    使用方法生成需要的对象
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }


    @Override
    public void getUser() {
        userDao.getUser();
    }
}
