package com.up.whyioc.dao.impl;

import com.up.whyioc.dao.UserDao;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-16 17:08
 **/
public class UserDaoMySqlImpl implements UserDao {
    @Override
    public void getUser() {
        System.err.println("从MySql中获取数据");
    }
}
