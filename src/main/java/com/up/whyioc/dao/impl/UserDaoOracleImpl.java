package com.up.whyioc.dao.impl;


import com.up.whyioc.dao.UserDao;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-16 17:15
 **/
public class UserDaoOracleImpl implements UserDao {
    @Override
    public void getUser() {
        System.err.println("从Oracle 中获取数据");
    }
}
