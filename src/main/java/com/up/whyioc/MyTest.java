package com.up.whyioc;

import com.up.whyioc.dao.impl.UserDaoMySqlImpl;
import com.up.whyioc.dao.impl.UserDaoOracleImpl;
import com.up.whyioc.service.impl.UserServiceImpl;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-16 19:17
 **/
public class MyTest {



    public static void main(String[] args) {
//        UserServiceImpl userService = new UserServiceImpl();
//        userService.getUser();

        UserDaoMySqlImpl userDaoMySql = new UserDaoMySqlImpl();
        UserServiceImpl userService = new UserServiceImpl();
        userService.setUserDao(userDaoMySql);
        userService.getUser();

//        切换实现类
        UserDaoOracleImpl userDaoOracle = new UserDaoOracleImpl();
        userService.setUserDao(userDaoOracle);
        userService.getUser();


    }
}
