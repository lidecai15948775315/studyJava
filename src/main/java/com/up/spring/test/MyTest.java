package com.up.spring.test;


import com.up.spring.bean.Person;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author: 李德才
 * @description:
 * @create: 2020-08-16 19:37
 **/
public class MyTest {

    public static void main(String[] args) {
        getPerson();
    }

    private static void getPerson() {
        /**
         * ApplicationContext 表示ioc 容器的入口，想要获取对象，必须创建该类
         * 该类有两个读取配置文件的是实现类
         * ClassPathXmlApplicationContext ： 表示从classpath中读取数据
         * FileSystemXmlApplicationContext: 表示从当前文件系统读取数据
         */
        /**
         * 容器中的对象是什么时候创建的？
         *           ------> 在 person 中添加无参构造方法
         * 容器中的对象在容器创建完成之前就已经把对象创建好了
         */
//        ApplicationContext context = new ClassPathXmlApplicationContext("ioc.xml");
        /**
         * //        直接使用对象的方式
         *         Person person = new Person();
         *         System.err.println(person);
         */
        /**
         * 获取具体的bean实例对象，需要强制类型转换
         */
//        Person person1 = (Person) context.getBean("person");
//        System.err.println(person1);
        /**
         * 获取对象的时候不需要强制类型转换
         */
//        Person person = context.getBean("person", Person.class);
//        System.err.println(person);


    }
}
